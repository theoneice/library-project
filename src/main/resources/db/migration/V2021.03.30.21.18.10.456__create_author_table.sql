CREATE TABLE authors (
    id bigserial NOT NULL,
    first_name character varying(250) NOT NULL,
    last_name character varying(250) NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    version bigint,
    CONSTRAINT author_pkey PRIMARY KEY (id)
);