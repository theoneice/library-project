CREATE TABLE books (
    id bigserial NOT NULL,
    title character varying(250) NOT NULL,
    date_of_publish timestamp with time zone NOT NULL,
    isbn character varying(50) NOT NULL,
    price numeric(8, 2) NOT NULL,
    version bigint,
    CONSTRAINT city_pkey PRIMARY KEY (id)
);